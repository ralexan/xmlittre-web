xmlittre-web
============

This is the source code for the site dedicated to the Littré dictionary.

* Coded in Haskell.
* Uses the framework [Yesod](http://www.yesodweb.com/).
* Uses [Sphinx Search](http://sphinxsearch.com/) for full-text search.
* Uses [Sqlite](http://www.sqlite.org) for the DB.

The DB schema, the todo list, and some of the variables are in French.

The license for the code and the documentation is
[Affero GPL](http://www.gnu.org/licenses/agpl-3.0.html).


Compiling
---------

* hsenv can set up a virtual environment for cabal-install (optional).
* system libraries for compiling:
    aptitude install zlib1g-dev libicu-dev
* Install separately the non-library package: `cabal install happy`
* `cabal install --only-dependecies` (with cabal 1.16 and multi-core, add `-j`)
* Install the Yesod tools: `cabal install yesod-bin`
* The sqlite library must be patched so that comparisons with collations become possible:
    * patch in `sphinx/misc/` for the sqlite C lib.
    * `cabal install --force-reinstalls --reinstall -f systemlib persistent-sqlite`
* `yesod build` or `yesod devel`

When compiling on a VPS with only 512 MB of memory, `yesod` will fail without any message.
Compiling directly with `cabal`, or even `ghc`, may be necessary.

