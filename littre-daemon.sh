#! /bin/sh

DAEMON=xmlittre
NAME=xmlittre-web
DESC=xmlittre-web

MAINDIR=$(readlink -f `dirname "$0"`)
PIDPATH="$MAINDIR/tmp/run"
START_OPTS="--make-pidfile --background --no-close --chdir $MAINDIR"
DAEMON_OPTS="production --port 3000"

LOGPATH="$MAINDIR/tmp/log"
LOG="$LOGPATH/access.log"
LOG_ERR="$LOGPATH/error.log"

DAEMON="$MAINDIR/$DAEMON"
test -e $DAEMON || DAEMON="$MAINDIR/dist/build/xmlittre/xmlittre"
test -x $DAEMON || exit 0

test -e $LOGPATH || mkdir -p $LOGPATH || exit 1
test -e $PIDPATH || mkdir -p $PIDPATH || exit 1

set -e

. /lib/lsb/init-functions


daemon_log () {
	echo $(date "+%F %T $1") >> $LOG_ERR
}

uncompress_file () {
	local file
	file=$1
	if [ -e "$1" ];
	then
		echo $1
		return 0
	else
		echo "Uncompressing... " >&2
		test -e "$1.gz" && gunzip "$1.gz"
		test -e "$1.bz2" && bunzip2 "$1.bz2"
		test -e "$1.xz" && unxz "$1.xz"
	fi
	test -e "$1" && echo "$1"
	return 0
}

daemon_start () {
	start-stop-daemon --start --pidfile $PIDPATH/$NAME.pid \
		--exec $DAEMON $START_OPTS -- $DAEMON_OPTS >> $LOG 2>> $LOG_ERR || true
}

daemon_stop () {
	start-stop-daemon --stop --pidfile $PIDPATH/$NAME.pid \
		--exec $DAEMON && rm "$PIDPATH/$NAME.pid"
}


case "$1" in
  start)
	echo -n "Starting $DESC: "
	daemon_log "Starting $DESC"
	daemon_start
	echo "$NAME."
	;;
  stop)
	echo -n "Stopping $DESC: "
	daemon_log "Stopping $DESC"
	daemon_stop
	echo "$NAME."
	;;
  restart|force-reload)
	echo -n "Restarting $DESC: "
	daemon_log "Restarting $DESC"
	daemon_stop
	sleep 1
	daemon_start
	echo "$NAME."
	;;
  update)
	cd $MAINDIR
	test -d "$2" || exit 1
	NEWEXEC=$(uncompress_file "$2/xmlittre")
	NEWDATA=$(uncompress_file "$2/littre.sqlite")
	if [ "$NEWEXEC$NEWDATA" = "" ]; then
		echo "Nothing to update."
		exit 0
	fi

	echo -n "Updating $DESC from path $2: "
	daemon_log "Updating $DESC from path $2"
	echo -n "Stopping $DESC: "
	daemon_stop
	if [ "$NEWEXEC" != "" ]; then
		mv -f $DAEMON $DAEMON.OLD
		mv $NEWEXEC $DAEMON
		chmod u+x $DAEMON
	fi
	if [ "$NEWDATA" != "" ]; then
		mv -f xmlittre.sqlite3 xmlittre.sqlite3.OLD
		mv $NEWDATA xmlittre.sqlite3
		chmod a-w xmlittre.sqlite3
	fi
	echo -n "Starting $DESC: "
	daemon_start
	if [ "$NEWDATA" != "" ]; then
		make sphinx-index
	fi
	echo "$NAME."
	;;
  status)
	status_of_proc -p $PIDPATH/$NAME.pid "$DAEMON" xmlittre && exit 0 || exit $?
	;;
  *)
	echo "Usage: $NAME {start|stop|restart|status}" >&2
	echo "       $NAME update <from_path>" >&2
	exit 1
	;;
esac

exit 0
