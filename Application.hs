{-# OPTIONS_GHC -fno-warn-orphans #-}
module Application
    ( makeApplication
    , getApplicationDev
    , makeFoundation
    ) where

import Import
import Settings
import Yesod.Default.Config
import Yesod.Default.Main
import Yesod.Default.Handlers

import Control.Monad.Logger
    (runLoggingT)
import Network.Wai.Middleware.RequestLogger as RL
    (Destination (Logger),
     IPAddrSource (..),
     OutputFormat (..), destination,
     mkRequestLogger, outputFormat)
import System.Log.FastLogger
    (defaultBufSize, newStdoutLoggerSet)
import Network.Wai.Logger (clockDateCacher)

import Data.Default (def)
import Yesod.Core.Types (loggerSet, Logger (Logger))

import qualified Database.Persist
-- import Database.Persist.GenericSql (runMigration)
import Database.Persist.Sqlite
import qualified Database.Sqlite as Sqlite

import Network.HTTP.Client.Conduit (newManager)

-- Logging
-- import Control.Monad.Logger
-- import Control.Concurrent (forkIO, threadDelay)
-- import System.Log.FastLogger (newStdoutLoggerSet, defaultBufSize)
-- import Network.Wai.Logger (clockDateCacher)
-- import Yesod.Core.Types (loggerSet, Logger (Logger))
-- import Data.Default (def)

-- Import all relevant handler modules here.
-- Don't forget to add new modules to your cabal file!
import Handler.Home
import Handler.Definition
import Handler.Auteur
import Handler.Oeuvre
import Handler.Annexes
import Handler.SearchFullText


-- This line actually creates our YesodDispatch instance. It is the second half
-- of the call to mkYesodData which occurs in Foundation.hs. Please see the
-- comments there for more details.
mkYesodDispatch "App" resourcesApp

-- This function allocates resources (such as a database connection pool),
-- performs initialization and creates a WAI application. This is also the
-- place to put your migrate statements to have automatic database
-- migrations handled by Yesod.
makeApplication :: AppConfig DefaultEnv Extra -> IO (Application, Yesod.Default.Main.LogFunc)
makeApplication conf = do
    foundation <- makeFoundation conf

    -- Initialize the logging middleware
    logWare <- RL.mkRequestLogger def
        { RL.outputFormat = if development
                then RL.Detailed True
                else RL.Apache FromSocket
        , destination = RL.Logger $ loggerSet $ appLogger foundation
        }

    -- Create the WAI application and apply middlewares
    app <- toWaiAppPlain foundation
    let logFunc = messageLoggerSource foundation (appLogger foundation)
    return (logWare $ defaultMiddlewaresNoLogging app, logFunc)

-- | Loads up any necessary settings, creates the foundation datatype, and
-- performs some initialization.
makeFoundation :: AppConfig DefaultEnv Extra -> IO App
makeFoundation conf = do
    manager <- newManager
    s <- staticSite
    dbconf <- withYamlEnvironment "config/sqlite.yml" (appEnv conf)
              Database.Persist.loadConfig >>=
              Database.Persist.applyEnv

    sphinxconf <- withYamlEnvironment "config/sphinx.yml" (appEnv conf)
                   Database.Persist.loadConfig >>=
                   Database.Persist.applyEnv

    loggerSet' <- newStdoutLoggerSet defaultBufSize
    (getter, _) <- clockDateCacher
    let logger = Yesod.Core.Types.Logger loggerSet' getter

    let mkFoundation pc = App
            { settings = conf
            , getStatic = s
            , connPool = pc
            , httpManager = manager
            , persistConfig = dbconf
            , sphinxConfig = sphinxconf
            , appLogger = logger
            }

    -- We need a log function to create a connection pool. We need a connection
    -- pool to create our foundation. And we need our foundation to get a
    -- logging function. To get out of this loop, we initially create a
    -- temporary foundation without a real connection pool, get a log function
    -- from there, and then create the real foundation.
    let tempFoundation = mkFoundation $ error "connPool forced in tempFoundation"
        logFunc = messageLoggerSource tempFoundation (appLogger tempFoundation)

    putStrLn $ "SQLite: creating pool of " ++ (show $ sqlPoolSize dbconf) ++ " connections."
    p <- flip runLoggingT logFunc $ createSqlitePoolConn (dbconf :: Settings.PersistConfig) logFunc
    putStrLn "done."

    return $ mkFoundation p

-- for yesod devel
getApplicationDev :: IO (Int, Application)
getApplicationDev =
    defaultDevelApp loader (fmap fst . makeApplication)
  where
    loader = Yesod.Default.Config.loadConfig (configSettings Development)
        { csParseExtra = parseExtra
        }

createSqlitePoolConn :: (MonadIO m, MonadLogger m, MonadBaseControl IO m) => SqliteConf -> Yesod.Default.Main.LogFunc -> m ConnectionPool
createSqlitePoolConn (SqliteConf cs size) logFunc = do
    createSqlPool (customConn cs) size
  where
      customConn :: Text -> Yesod.Default.Main.LogFunc -> IO SqlBackend
      customConn s l = Sqlite.open s >>= execInit >>= (flip wrapConnection l)

      execInit :: Sqlite.Connection -> IO Sqlite.Connection
      execInit conn = execute conn "SELECT icu_load_collation('fr_FR', 'french_ci', 1)"
        
      execute :: Sqlite.Connection -> Text -> IO Sqlite.Connection
      execute conn sql = do
        stmt <- Sqlite.prepare conn sql
        _ <- Sqlite.step stmt
        Sqlite.finalize stmt
        return conn
